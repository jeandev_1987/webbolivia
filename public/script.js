// script navbar
document.addEventListener("DOMContentLoaded", function(){
  window.addEventListener('scroll', function() {
      if (window.scrollY > 50) {
        document.getElementById('navbar_top').classList.add('sticky');
        if(document.location.pathname === "/parque"){
          document.getElementById('navbar-logo').src="./logo.svg"
          document.getElementById('navbar-menu').src="./iconMenuB.svg";
        }
      } else {
        document.getElementById('navbar_top').classList.remove('sticky');
        if(document.location.pathname === "/parque"){
          document.getElementById('navbar-logo').src="./logoBlanco.svg";
          document.getElementById('navbar-menu').src="./iconMenuW.svg";
        }
        // document.body.style.paddingTop = '0';
      } 
  });
}); 
