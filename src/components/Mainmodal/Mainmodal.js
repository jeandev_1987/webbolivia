import React, { useState } from 'react';
import './Mainmodal.scss'
import ReactDOM from 'react-dom';
import lateralHomeModal from '../../assets/home/lateral-homemodal.png';
import isotipoHomeModal from '../../assets/navbar/logo.svg';

const Mainmodal = ({ isShowingMain, hideMain }) => {
  const [showMessageMain, setShowMessageMain] = useState(false);

  return (

    isShowingMain ? ReactDOM.createPortal(
      <React.Fragment>
        <div className="modal-overlay2" />
        <div className="modal-wrapper2" aria-modal aria-hidden tabIndex={-1} role="dialog">
          <div class="modal-dialog modal-dialog-centered-main modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="btn-close _btn-close-white" onClick={hideMain} ></button>
                <div class="modal-header-center">
                  <img src={lateralHomeModal} class="lateralhomemodal" alt="..."/>
                  <h1>
                    Comunicado
                  </h1>
                  <br></br>
                  <p class="body-font">
                    El Manantial Inversiones S.A. administradora del Cementerio Memorial Park,
                    comunica a todas aquellas personas que tienen contratos con nosotros y que hayan cumplido
                    con el pago de la totalidad de las cuotas pactadas, puedan apersonarse por nuestras oficinas 
                    a objeto de conciliar e iniciar trámite de su respectiva minuta de transferencia definitiva.
                  </p>

                  <p class="body-font">
                    Para cualquier consulta por favor contáctanos al teléfono 336-1920 o visitarnos en nuestras 
                    oficinas de la Prolongación Campero esq. Acre #194.
                  </p>

                  <div style={{height:'70px', display:'flex', flexDirection:'row', justifyContent:'flex-end', alignItems:'flex-end'}}>
                    <img src={isotipoHomeModal} alt="" class="logo"></img>
                  </div>
                </div>

                {/* <div class="buttons">
                  <button type="button" class="btn btn-secondary btn-sm" onClick={hideMain}>ENTENDIDO &nbsp;</button>
                </div> */}
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>, document.getElementById('modal')
    ) : null
  )
}
export default Mainmodal;
