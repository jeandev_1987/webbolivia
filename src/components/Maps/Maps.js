import React from 'react';
import GoogleMaps from 'simple-react-google-maps';
//import GoogleMapReact from "google-map-react";
import jsPDF from 'jspdf';
import iconLocation from '../../assets/home/iconLocation.svg'; 
import iconDownload from '../../assets/sepultados/iconDownload.svg'; 

function Maps(datos) {
    console.log(datos)
    let coord= datos?.datos?.latlon?.split(",");
    console.log(coord);
    let lat= parseFloat(coord? coord[0] : null)
    console.log(lat)
    let lng= parseFloat(coord? coord[1] : null)
    console.log(lng);
    let name= datos?.datos?.nombres
    let nameOb= datos?.datos?.nombre
    let apellido= datos?.datos?.apaterno
    let fecha= datos?.datos?.fechasep
    let hora= datos?.datos?.hora
    let sec= datos?.datos?.sec
    let sep= datos?.datos?.sep

    const generatePDF = ()=> {
      let doc = new jsPDF ("p", "pt", "a4"); 
      doc.html(document.querySelector("#maps"),{
          callback: function(pdf){
              pdf.save("maps.pdf");
          }
      });
}

    return (

        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <div class="rows">
                                <table style={{ width:'100%'}}>
                                    <tr>
                                        <td style={{ width:'45%'}}>
                                            <div>
                                                <h7 class="modal-title" id="exampleModalLabel">Nombre {name} {apellido} {nameOb}</h7>
                                            </div>
                                        </td>
                                        <td style={{ width:'55%'}}>
                                            <div class="input-group d-flex justify-content-end mt-2 mb-3">
                                                <button type="button" class="btn btn-primary btn-sm btnDownload iconDownloadPC" onClick={generatePDF}>DESCARGAR</button>
                                                <button type="button" class="btn btn-primary btn-sm btnDownload iconDownloadMobile" onClick={generatePDF}><img src={iconDownload} alt="" /></button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style={{ width:'45%'}}>
                                            <div>
                                                <h7 class="modal-title" id="exampleModalLabel"> {fecha ? `Fecha de sepultura: ${fecha}` : null} {hora ? `Horario: ${hora}` : null} </h7><br/>
                                                <h7 class="modal-title" id="exampleModalLabel"> {`Sector: ${sec}`} </h7><br/>
                                                <h7 class="modal-title" id="exampleModalLabel"> {`Sepultura: ${sep}`} </h7>
                                            </div>
                                        </td>
                                        <td className="btnLlegarPC" style={{ width:'55%'}}>
                                            <div class="input-group d-flex justify-content-end mt-2 mb-3">
                                                <div class="input-group-text btnGroupAddon"><img src={iconLocation} alt="" /></div>
                                                {/* <input type="button" class="form-control" placeholder="VER COMO LLEGAR" aria-label="Input group example" aria-describedby="btnGroupAddon"/> */}
                                                <a href="https://www.google.com/maps/dir//-17.7629335,-63.0299836/@-17.762933,-63.029984,841m/data=!3m1!1e3?hl=es-419" target="_blank">
                                                <button type="button" class="btn btn-primary btn-sm btnLlegar">VER COMO LLEGAR</button>
                                                </a>
                                            </div>
                                        </td>
                                        <td className="btnLlegarMobile" style={{ width:'55%'}}>
                                            <div class="input-group d-flex justify-content-end mt-2 mb-3">
                                                <p className="horaPopup">{hora ? `${hora}` : null}</p>
                                            </div>
                                            <div class="input-group d-flex justify-content-end mt-2 mb-3 btnUbicacion">
                                                <div class="input-group-text btnGroupAddon"><img src={iconLocation} alt="" /></div>
                                                {/* <input type="button" class="form-control" placeholder="VER COMO LLEGAR" aria-label="Input group example" aria-describedby="btnGroupAddon"/> */}
                                                <a href="https://www.google.com/maps/dir//-17.7629335,-63.0299836/@-17.762933,-63.029984,841m/data=!3m1!1e3?hl=es-419" target="_blank">
                                                <button type="button" class="btn btn-primary btn-sm btnLlegar">VER COMO LLEGAR</button>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <div class="container" id="maps" style={{ height: "400px", width: "100%" }}>

                            <GoogleMaps
                                style={{ height: "400px", with: "100%" }}
                                zoom={16}
                                center={{lat: -17.7621690895085, lng: -63.0309437275737}}
                                //center={{ lat: lat, lng: lng }}
                                apiKey={"AIzaSyCwu6vtJb6jj5fg7jnGxq-m7r6I6sr_Se0"}
                                markers={{ lat: lat.lat, lng: lat.lng}}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>


    )
}

export default Maps
