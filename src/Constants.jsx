// import Noticias1 from './constantes'; 
// import Noticias2 from './constantes'; 
// import Noticias3 from './constantes'; 
// import Noticias4 from './constantes'; 

import imgNews1 from './assets/home/Noticias1.svg';
import imgNews2 from './assets/home/Noticias4.svg';
import imgNews3 from './assets/home/Noticias3.svg';
import imgNews4 from './assets/home/Noticias2.svg';


//----------------PREGUNTAS FRECUENTES-----------------

export const QuestionsGenerales = [
    {id:1, question: '¿Cuáles son los horarios de atención de Memorial Park?', response: 'Puede encontrar los horarios de atención de Memorial Park y de nuestra oficina comercial en el siguiente link https://www.memorialpark.bo/informacióngeneral'},
    {id:2, question: '¿Cómo puedo conocer la ubicación de un fallecido del Cementerio Parque?', response: 'Puede buscar la ubicación de un fallecido ingresando el Nombre, Apellido, CI o NIT, en el siguiente link https://www.memorialpark.bo/ubicacion-sepultados/'}
]
export const QuestionsSepultaciones = [
    {id:1, question:'¿Cuáles son los pasos a seguir ante el fallecimiento de un ser querido?', response: 'Para coordinar la sepultación de un fallecido deberá contar con algunos documentos e información del servicio funerario. El detalle de esta información la puede encontrar en el siguiente link https://www.memorialpark.bo/en-caso-de-fallecimiento Una vez que usted cuente con toda la documentación, debe acercarse a Memorial Park para realizar el trámite de forma presencial.'}
]

export const QuestionsReducciones = [
    {id:1, question: '¿Qué es una reducción?', response: 'La reducción es la acción de reducir el cuerpo de un fallecido, de manera que sus restos pasen a ocupar sólo una parte del espacio correspondiente al féretro en que fueron inhumados.'}
]
export const QuestionsVentas= [
    {id:1, question:'¿Qué ventaja tiene adquirir una sepultura anticipada?', response: 'Adquirir una sepultura de manera anticipada puede entregar tranquilidad y beneficios a toda la familia. Además de esta tranquilidad, la compra anticipada le da acceso a menores valores de compra para uso inmediato y le permite realizar el pago en cuotas, acorde a su necesidad. Para conocer los distintos servicios que ofrece Memorial Park puede ingresar en el siguiente https://www.memorialpark.bo/elparque'},
    {id:2, question:'¿Cuáles son los valores para las sepulturas?', response: 'Contamos con diferentes precios que van desde los $us. 2.950 hasta los $us. 6.000 dependiendo la ubicación. *Precio de NF'},
    {id:3, question:'¿Dónde puedo pagar las cuotas de crédito y cuotas de mantención?', response: 'Puede pagarlas en nuestras oficinas centrales a través de transferencia Bancaria o en nuestros puntos de pagos externos. (aquí se podría poner la información de puntos de pagos)'},
    {id:4, question:'¿Puedo pagar de manera presencial?', response: 'Puede hacerlo, en nuestra oficina central, Dirección...'}
]
export const QuestionsInstalaciones= [
    {id:1, question: '¿Cuentan con Capilla y Velatorio?', response: 'No por el momento.'},
    {id:2, question:'¿Puedo solicitar una ceremonia para conmemorar a un fallecido?', response: 'Puede solicitarlo en Atención al Cliente en nuestra oficina Central el cual tiene un costo de $us. 25'}
] 


//------------------------NOTICIAS-----------------------------------------

export const AllNews=[
    {id:1, image: imgNews1, fecha: '21/09/2019', titulo: "Planta tu amor por Santa Cruz y Fauna chiquitana", cuerpo: "Memorial Park agradece a DESAFÍO FAUNA CHIQUITANA por haber ayudado a que nuestra actividad “Planta tu amor por Santa Cruz” que se realiza todos los años, en esta oportunidad haya tenido un fin benéfico para la Chiquitania que tanto nos necesita. También agradecemos a las miles de personas que realizaron sus donativos a cambio de plantines, los cuales serán gestionados por Desafío Fauna Chiquitana."},
    {id:2, image: imgNews2, fecha: '04/02/2020', titulo: "Reforestación de nuestros bosques", cuerpo: "Reafirmando nuestro compromiso con la naturaleza, el día sábado participamos de la tercera precarnavalera junto a la comparsa coronadora 2020 “Chiripas jr.”, con quienes repartimos 400 plantines. Agradecemos a los coronadores por la invitación y por incentivar a la reforestación de nuestros bosques."},
    {id:3, image: imgNews3, fecha:'05/10/2020', titulo: "Plantar un árbol es plantar vida", cuerpo: "Reafirmando nuestro compromiso con la Naturaleza, el día sábado 03 de octubre participamos de la campaña de Medio Ambiente “Plantar un árbol es plantar vida”, organizado por la Asociación Internacional de Clubes de Leones. Hicimos la entrega de 60 plantines, los cuales fueron plantados en Satélite Norte, y embellecerán aún más a Santa Cruz. Memorial Park comprometidos con la naturaleza y su familia"},
    {id:4, image: imgNews4, fecha:'31/10/2020', titulo: "Salvando vidas", cuerpo: "Porque nuestro compromiso es con usted, su familia y la naturaleza…Por eso nos sentimos felices y orgullosos de hacer la entrega al Hospital Municipal de la Ciudad de Cotoca, a través de nuestra campaña SALVANDO VIDAS, 3 ambulancias cero (0) Km debidamente equipadas para la lucha contra el COVID-19."}

]